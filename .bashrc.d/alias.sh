alias v='vim'


alias ls='ls -A --color=auto' # --color is not POSIX compliant
alias lsl='ls -l -A -h --color=auto' # -h and --color are not POSIX compliant

alias grep='grep --color=auto'


alias b='cat ~/Documents/bookmarks/bookmarks.txt'


alias gis='git status'
alias gia='git add .'
alias gic='git commit'
alias gicm='git commit -m'
alias gid='git diff'
alias gidd='git diff --cached'
alias gips='git push'
alias gipu='git pull'
alias gil='git log --date=iso'


alias dni='sudo dnf install '
alias dniy='sudo dnf -y install '
alias dnr='sudo dnf remove '
alias dnu='sudo dnf update '
alias dnuy='sudo dnf -y update '
alias dns='sudo dnf search '


alias api='sudo apt install '
alias api='sudo apt -y install '
alias apu='sudo apt update && sudo apt -y upgrade'
alias app='sudo apt purge '
alias aps='apt search '
alias apup='sudo apt update '
alias apug='sudo apt upgrade '
alias apl='apt list --upgradable'
alias apr='sudo apt autoremove'


alias pdflatex='mkdir -p output; pdflatex -output-directory=output/ '
