# '\[' and '\]' at the end and the beginning, respectively, fix strange overlap
# errors, for me.
txtgrn='\[\e[01;32m\]' # Green
txtrst='\[\e[0m\]' # Text Reset
PS1="$txtgrn[\u@\h \W]\$ $txtrst"
