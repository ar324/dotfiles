# Usage: backup [hint] [path_of_file_to_back_up]
# Format of created file: backup-2021-12-14-"$hint".tar.gpg
backup() {
	if [ "$1" = "" ] || [ "$2" = "" ]; then
		echo "Usage: backup [hint] [path_of_file_to_back_up]"
		return
	fi
	response= # If previously set. Where are scopes?
	fname='backup-'$(date +%Y-%m-%d)"-$1"'.tar'
	gpg_fname="$fname.gpg"
	# Go to folder's parent directory, create an archive there, and move the
	# archive back to the current directory. -C is not working for reasons I
	# have been unsuccessful in debugging, so I am just letting it be for
	# now.
	pdir=$(dirname "$2") || return
	bname=$(basename "$2") || return
	cdir=$(pwd) || return
	cd "$pdir" || return
	tar -cf "$fname" "$bname" || return
	echo "TAR archive $fname created."
	mv "$pdir/$fname" "$cdir" # No '|| return' as that would cause issues
		# if cdir == pdir
	cd "$cdir" || return
	while [ "$response" != 'y' ] && [ "$response" != 'n' ]; do
		printf 'Encrypt archive? (y/n) '
		read -r response
	done
	if [ "$response" = 'y' ]; then
		gpg -c "$fname" || return
		echo "Encrypted archive $gpg_fname created."
		response=
		while [ "$response" != 'y' ] && [ "$response" != 'n' ]; do
			printf "Delete non-encrypted archive %s? (y/n) " "$fname"
			read -r response
		done
		if [ "$response" = 'y' ]; then
			rm "$fname" || return
			echo "Deleted non-encrypted archive."
		fi
		echo "Reloading gpg-agent . . ."
		gpgconf --reload gpg-agent || return
	fi
	echo "All done. Bye..."
}

# Tip: Set up a keyboard shortcut for "bash -ic 'bmrk'" and
# cat ~/Documents/bookmarks/bookmarks.txt
bmrk() {
	bookmark="$(wl-paste)"
	echo "$bookmark" >> ~/Documents/bookmarks/bookmarks.txt
	notify-send -e "Bookmarked $bookmark" # -e = transient notification
}
