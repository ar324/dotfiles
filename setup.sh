sudo cp ./xfce.jpg /usr/share/backgrounds/xfce.jpg

gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize-or-previews'
# The inbuilt screenshot tool does not support custom folders as of now. Install gnome-screenshot.
gsettings set org.gnome.gnome-screenshot auto-save-directory "file:///home/$USER/Documents/unprocessed/screenshots/"

# Set up symbolic links.
rm ~/.bashrc.d
ln -s $PWD/.bashrc.d ~/.bashrc.d
mkdir ~/.vim
ln -s $PWD/.vim/vimrc ~/.vim/vimrc
rm -rf ~/.ssh
ln -s $PWD/../auth/.ssh ~/.ssh
mkdir ~/.config
ln -s $PWD/foot ~/.config/foot
mkdir -p ~/.config/Code/User
ln -s $PWD/settings.json ~/.config/Code/User/settings.json

# Spoof device's MAC address. Works only with systems that use Network Manager.
sudo mkdir -p /etc/NetworkManager/conf.d
echo '[connection]
wifi.cloned-mac-address=random
ethernet.cloned-mac-address=random' | sudo tee /etc/NetworkManager/conf.d/spoof-mac.conf
sudo systemctl restart NetworkManager


# Fetch only HTTPS-enabled package mirrors.
sudo sed -i -- '/^metalink=/s/$/\&protocol=https/' /etc/yum.repos.d/fedora*.repo


# Install the RPMFusion repository.
# Signing key fingerprint: E9A4 91A3 DE24 7814 E7E0 67EA E06F 8ECD D651 FF2E.
sudo dnf install "https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm"

# Fetch only HTTPS-enabled package mirrors for RPMFusion.
sudo sed -i -- '/^metalink=/s/$/\&protocol=https/' /etc/yum.repos.d/rpmfusion*.repo


# Add Flathub repository to Flatpak.
flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak --user install flathub net.ankiweb.Anki org.signal.Signal


# Remove unnecessary preinstalled packages (Fedora).
sudo dnf remove totem rhythmbox # GNOME
sudo dnf remove kmahjongg kpat kmines dragon plasma-discover dnfdragora # KDE
sudo dnf remove xterm xfdashboard dnfdragora xfburn asunder geany parole pragha # XFCE

# Remove unnecessary packages (Ubuntu).
sudo apt purge thunderbird totem aisleriot gnome-mines rhythmbox

# Install useful packages (Fedora).
sudo dnf install git vim foot keepassxc mozilla-ublock-origin goldendict foliate golang perl-Image-ExifTool wl-clipboard ripgrep gnome-tweaks vlc obs-studio libreoffice-calc libreoffice-writer libreoffice-impress ImageMagick php shotwell gnome-extensions-app gnome-shell-extension-appindicator gnome-shell-extension-dash-to-dock transmission-gtk texlive-collection-fontsrecommended gnome-screenshot # xclip

# Install useful packages (Ubuntu).
sudo apt install texlive texlive-latex-extra git vim foot flatpak keepassxc goldendict golang libimage-exiftool-perl wl-clipboard ripgrep gnome-tweaks vlc obs-studio imagemagick php

go install codeberg.org/ar324/foototp@latest
echo ~/Documents/auth/foototp.json | foototp configure

sudo ln -s /var/lib/snapd/snap /snap
sudo systemctl restart snapd
sudo snap install code --classic

# Python development environment
python3 -m pip install pylint\
    pycodestyle \
    mypy \
    autopep8
