set -o vi
# Do not need the following any more, because I now virtually swap the "caps lock" key and the "escape" key.
# bind '"kj":vi-movement-mode'
