export PATH=$PATH:~/.local/bin


export GOPATH=~/.local/share/go

export PATH=$PATH:$GOPATH/bin

export GOPROXY=direct
export GOSUMDB=off


export PATH=$PATH:~/.local/share/cargo/bin
